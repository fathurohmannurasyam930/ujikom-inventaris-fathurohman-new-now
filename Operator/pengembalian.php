<?php
include"header.php";
include 'database/class.php';
$db = new database();
?>
<header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Pengembalian</h3>
                        </div>
                    </div> 
                </div>
            </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                        

                     <div class="row">
                   
                    <div class="col-md-12">
                    <div class="au-card recent-report">
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <strong>Data Peminjaman & Pengembalian Barang</strong>
                                            </div>

                                            <div class="col-md-6" align="right">
                                         <a href="import/laporan_pinjam.php">
                                        <button class="btn btn-danger btn-sm">
                                            <i class=""></i>CETAK & EXPORT</button>
                                        </a>
                                            </div>

                                       
                                         
                                        </div>
                                   
                                     <hr>
                                         <div class="row">
                                             <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table width="100%" class="table table-data2" id="dataTables">
                                                        <thead align="center">
                                                            <tr>
                                                                <th><center>No</center></th>
                                                                <th><center>Kode Peminjaman</center></th>
                                                                <th><center>Nama Pegawai</center></th>
                                                                <th><center>Tanggal&Waktu Peminjaman</center></th>
                                                                <th><center>Pengembalian</center></th>
                                                                <th><center>Status</center></th>
                                                                
                                                                <th><center>Opsi</center></th>
                                                            </tr>
                                                        </thead align="center">
                                                        <tbody align="center">
                                                                <?php
                                                                $no = 1 ;
                                                                foreach($db->peminjam() as $x){
                                                                ?>
                                                                <tr>
                                                                    <td><br><?php echo $no++;?></td>
                                                                    <td><?php echo $x['kode_peminjaman'];?></td>
                                                                    <td><?php echo $x['kode_pegawai'];?></td>
                                                                    <td><?php echo $x['tanggal_pinjam']; ?></td>
                                                                    <td>
                                                                      <a href="pro_inven.php?jumlah=<?php echo $x['jumlah'];?>&kode_peminjaman=<?php echo $x['kode_peminjaman'];?>&kode_inventaris=<?php echo $x['kode_inventaris'];?>&aksi=kembali">
                                                                        <button type="reset" class="btn btn-primary btn-sm">
                                                                            <i class=""></i> Kembalikan
                                                                        </button>
                                                                        </a>
                                                                    </td>
                                                                    <td><?php echo $x['status_peminjaman']; ?></td>
                                                                   
                                                                    <td width="100%">
                                                                        <center>
                                                                        <a href="liat_pinjam.php?kode_peminjaman=<?php echo $x['kode_peminjaman'];?>&kode_inventaris=<?php echo $x['kode_inventaris'];?>">
                                                                        <button type="reset" class="btn btn-success btn-sm">
                                                                            <i class="fas fa-eye"></i> Detail
                                                                        </button>
                                                                        </a>
                                                                        <br>
                                                                        <br>
                                                                        <a href="pro_inven.php?kode_peminjaman=<?php echo $x['kode_peminjaman']; ?>&aksi=hapus_pinjam">
                                                                        <button type="reset" class="btn btn-danger btn-sm">
                                                                            <i class="fa fa-ban"></i> Hapus
                                                                        </button>
                                                                        </a>
                                                                        </center>
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                                }
                                                                ?> 
                                                        </tbody>
                                                    </table>
                                                    
                                                     
                                                    <hr>
                                                </div>
                               
                                            </div>
                                         </div>
                                    </div>
                    
                    </div> 
                </div>
            </div>

           

                <?php
                include"footer.php";
                ?>
                
        </div>
    </div>

    
</body>
</html>
<!-- DataTables JavaScript -->
    <script src="DT/jquery.dataTables.min.js"></script>
    <script src="DT/dataTables.bootstrap.min.js"></script>



<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
                responsive: true
        });
    });
 </script>