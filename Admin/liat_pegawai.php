<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Detail Pegawai</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Detail</strong> Pegawai
                                        </div>
                                        <div class="card-body card-block">
                                                <?php
                                                        include"database/koneksi.php";
                                                        $kode_pegawai=$_GET['kode_pegawai'];
                                                        $pilih=mysqli_query($koneksi, "SELECT * FROM pegawai WHERE kode_pegawai='$kode_pegawai'");
                                                        $tampil=mysqli_fetch_array($pilih);
                                                ?>
                                                <input type="hidden" value="<?php echo $_GET['kode_pegawai'];?>">
                                                <table class="table table-borderless table-striped table-earning" id="dataTables">
                                                        <tr>
                                                            <td>Kode Pegawai</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['kode_pegawai'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>NIP</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['nip'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Pegawai</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['nama_pegawai'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kelas</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['kelas'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Username</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['username'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td>
                                                            <td> : </td>
                                                            <td> <?php echo $tampil['password'];?> </td>
                                                        </tr>
                                                </table>
                                               
                                                    <br>

                                                <div class="card-footer">
                                                    <a href="pengguna.php"><input class="btn btn-primary btn-sm" type="submit" name="" value="Oke"></a>
                                                </div>

                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>