<?php
include"header.php";
?>
            <header class="header-desktop">

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            
                           <h3>Detail Barang</h3>

                        </div>
                    </div>
                </div>
                </header>

            <div class="main-content">
                <div class="section__content section__content--p30">

                 <div class="row" align="center">
                   
                    <div class="col-lg-12 ">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Detail</strong> Barang
                                        </div>
                                        <div class="card-body card-block">
                                                <?php
                                                        include"database/koneksi.php";
                                                        $kode_inventaris=$_GET['kode_inventaris'];
                                                        $pilih=mysqli_query($koneksi, "SELECT * FROM inventaris WHERE kode_inventaris='$kode_inventaris'");
                                                        $tampil=mysqli_fetch_array($pilih);
                                                ?>
                                                <input type="hidden" value="<?php echo $_GET['kode_inventaris'];?>">
                                                <table class="table table-borderless table-striped table-earning" id="dataTables">
                                                        <tr>
                                                            <td>Kode Inventaris</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['kode_inventaris'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nama Barang</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['nama_barang'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jenis</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['jenis'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Kondisi</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['kondisi'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jumlah</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['jumlah'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ruang</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['ruang'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Jurusan / Asal Barang</td>
                                                            <td> : </td>
                                                            <td> <?php echo $tampil['asal_barang'];?> </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Keterangan</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['keterangan'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Perugas</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['petugas'];?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tanggal Register</td>
                                                            <td> : </td>
                                                            <td><?php echo $tampil['tanggal_register'];?></td>
                                                        </tr>
                                                </table>
                                               
                                                    <br>

                                                <div class="card-footer">
                                                    <a href="inventarisir.php"><input class="btn btn-primary btn-sm" type="submit" name="" value="Oke"></a>
                                                </div>

                                        </div>
                                        
                                    </div>
                    </div> 
                </div>

              

                </div>
            </div>
            <hr>

                <?php
                include"footer.php";
                ?>
                
</div>
</div>
</body>
</html>