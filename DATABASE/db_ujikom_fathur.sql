-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09 Apr 2019 pada 06.45
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ujikom_fathur`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `asal_barang`
--

CREATE TABLE `asal_barang` (
  `kode_asalbarang` varchar(200) NOT NULL,
  `asal_barang` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `asal_barang`
--

INSERT INTO `asal_barang` (`kode_asalbarang`, `asal_barang`) VALUES
('KAB01', 'Animasi'),
('KAB02', 'Rekayasa Perangkat lunak'),
('KAB03', 'Broadcasting'),
('KAB04', 'Teknik Kendaran Ringan'),
('KAB05', 'Teknik Pengelasan'),
('KAB06', 'TU'),
('KAB07', 'Musolah'),
('KAB08', 'Guru'),
('KAB09', 'Perpustakaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `kode_detail_pinjam` varchar(200) NOT NULL,
  `kode_peminjaman` varchar(200) NOT NULL,
  `kode_inventaris` varchar(200) NOT NULL,
  `jumlah` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `kode_inventaris` varchar(200) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `asal_barang` varchar(200) NOT NULL,
  `kondisi` varchar(200) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `ruang` varchar(20) NOT NULL,
  `tanggal_register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `keterangan` varchar(200) NOT NULL,
  `petugas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`kode_inventaris`, `nama_barang`, `asal_barang`, `kondisi`, `jumlah`, `jenis`, `ruang`, `tanggal_register`, `keterangan`, `petugas`) VALUES
('KI01', 'Laptop', 'Rekayasa Perangkat lunak', 'Baik', 100, 'Elektronik', 'Lab RPL 1', '2019-04-09 04:45:22', '-', 'Asyam'),
('KI02', 'Infokus', 'Semua Kalangan', 'Baik', 100, 'Infokus', 'TU', '2019-04-09 04:44:39', '-', 'Asyam'),
('KI03', 'Papan Tulis', 'Animasi', 'Baik', 100, 'Hp', 'Studio Animasi', '2019-04-09 04:41:53', '-', 'Asyam'),
('KI04', 'Speker', 'TU', 'Baik', 100, 'Hp', 'TU', '2019-04-06 06:58:55', '-', 'Raja'),
('KI05', 'Headset', 'Brocasting', 'Baik', 100, 'Hp', 'Lab Animasi', '2019-04-09 04:41:57', '-', 'Raja'),
('KI06', 'Meja', 'Guru', 'Baik', 100, 'Infokus', 'Perpustakaan', '2019-04-06 06:59:09', '-', 'Maudhio Andre'),
('KI07', 'Buku', 'Perpustakaan', 'Baik', 100, 'Hp', 'Perpustakaan', '2019-04-06 04:55:58', '-', 'Fathurohman Nurasyam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `kode_jenis` varchar(200) NOT NULL,
  `nama_jenis` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`kode_jenis`, `nama_jenis`, `keterangan`) VALUES
('0KJ1', 'Elektronik', '-'),
('0KJ2', 'Alat Sekolah', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `kode_level` varchar(200) NOT NULL,
  `nama_level` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`kode_level`, `nama_level`) VALUES
('KL01', 'Admin'),
('KL02', 'Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `kode_pegawai` varchar(200) NOT NULL,
  `nip` char(200) NOT NULL,
  `nama_pegawai` varchar(200) NOT NULL,
  `kelas` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`kode_pegawai`, `nip`, `nama_pegawai`, `kelas`, `username`, `password`) VALUES
('KPG01', '088080', 'M Rifai', '12RPL3', 'Rifai', 'User123'),
('KPG02', '090909', 'Fathurohman Nurasyam', '12RPL3', 'FN99', 'User123'),
('KPG03', '808080', 'M Raja fahri', '12RPL3', 'Raja', 'User123'),
('KPG04', '909090', 'Maudhio', '12RPL1', 'Maudhio', 'User123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `kode_peminjaman` varchar(200) NOT NULL,
  `kode_detail_pinjam` varchar(200) NOT NULL,
  `kode_inventaris` varchar(255) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `tanggal_pinjam` datetime NOT NULL,
  `tanggal_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(200) NOT NULL,
  `kode_pegawai` varchar(200) NOT NULL,
  `kelas` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`kode_peminjaman`, `kode_detail_pinjam`, `kode_inventaris`, `jumlah`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `kode_pegawai`, `kelas`) VALUES
('KP62', 'KDP17', 'Ki02', 0, '2019-04-09 06:44:18', '2019-04-09 06:44:39', 'Telah DiKembalikan', 'KKK', '11 RPL 3'),
('KP64', 'KDP90', 'KI01', 0, '2019-04-09 06:42:46', '2019-04-09 06:43:07', 'Telah DiKembalikan', 'KKK', '11 RPL 2'),
('KP97', 'KDP96', 'Ki01', 0, '2019-04-09 06:45:09', '2019-04-09 06:45:22', 'Telah DiKembalikan', 'KKK', '11 RPL 3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `kode_petugas` varchar(15) NOT NULL,
  `nama_petugas` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`kode_petugas`, `nama_petugas`, `username`, `password`, `level`) VALUES
('KPTG01', 'Fathurohman Nurasyam', 'FN99', 'Admin', 'Admin'),
('KPTG02', 'M Raja ', 'Raja', 'Operator', 'Operator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `kode_ruang` varchar(200) NOT NULL,
  `nama_ruang` varchar(200) NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`kode_ruang`, `nama_ruang`, `keterangan`) VALUES
('KR01', 'Lab Animasi', '-'),
('KR02', 'Lab RPL 1', '-'),
('KR03', 'Studio Animasi', '-'),
('KR04', 'TU', '-'),
('KR05', 'Lab RPL 2', '-'),
('KR06', 'Musolah', '-'),
('KR07', 'Perpustakaan', '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asal_barang`
--
ALTER TABLE `asal_barang`
  ADD PRIMARY KEY (`kode_asalbarang`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`kode_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD UNIQUE KEY `kode_inventaris` (`kode_inventaris`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD UNIQUE KEY `kode_jenis` (`kode_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`kode_level`),
  ADD UNIQUE KEY `id_level` (`kode_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`kode_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD UNIQUE KEY `id_petugas` (`kode_petugas`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD UNIQUE KEY `kode_ruang` (`kode_ruang`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
